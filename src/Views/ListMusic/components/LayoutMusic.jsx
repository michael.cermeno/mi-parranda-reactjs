import React from 'react'
import './layoutMusic.css'
function LayoutMusic() {
    return (
        <div className='layout-box'>
            <div className="boxes">
                <div className="photo-accor">
                    <h1>Nuestro Repertorio</h1>
                </div>
                <div className="canciones">
                    <h1>Canciones Mas Sugeridas</h1>
                    <div className="box-list">
                        <div className="list">
                            <p>
                                <ul>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=UvO-YwQpzFQ" rel="noopener noreferrer"
                                            target="_blank">LO AJENO SE RESPETA – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=k7UXpV20cyk" rel="noopener noreferrer"
                                            target="_blank">EL TERREMOTO – Martin elias</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=17TSdx1mCYkstyle=" rel="noopener noreferrer"
                                            target="_blank">LOCO PARANÓICO – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=V8g7pvWw6nM" rel="noopener noreferrer"
                                            target="_blank">ESPOSA MÍA – Otto Serge</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=AvKLcf9g27M" rel="noopener noreferrer"
                                            target="_blank">LA CIQUITRILLA- Silvestre Dangond </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=DeBo21VT52A" rel="noopener noreferrer"
                                            target="_blank"> ABRETE – Martin Elias</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=DyOUwI1F5Pk" rel="noopener noreferrer"
                                            target="_blank">SE ACABARON YA- Silvestre Dangond </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=j-N24BsF9N4" rel="noopener noreferrer"
                                            target="_blank">TRAGADO DE TI – Peter manjarrés</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=eYbG5wqjTfM" rel="noopener noreferrer"
                                            target="_blank">AUNQUE DESPUÉS ME DUELA – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=0UYBiyox24w" rel="noopener noreferrer"
                                            target="_blank">EL ORIGINAL – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=ygfE6bmdEu8" rel="noopener noreferrer"
                                            target="_blank">LA CASA EN EL AIRE – Escalona</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=PjqMtjNRCNY" rel="noopener noreferrer"
                                            target="_blank">LA GOTA FRÍA – Carlos Vives</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=MXul-dAdEj4" rel="noopener noreferrer"
                                            target="_blank">GAVIOTA HERIDA – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=7Am3C8b1Hxs" rel="noopener noreferrer"
                                            target="_blank">EL AMOR MÁS GRANDE DEL PLANETA – Pipe Peláez</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=NkB_Q2ThDBY" rel="noopener noreferrer"
                                            target="_blank">LA TIERRA DEL OLVIDO – Carlos Vives</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=Yar7CEp8WRU" rel="noopener noreferrer"
                                            target="_blank">PARRANDA EN EL CAFETAL – jorge Celedon </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=28RH3B7a80c" rel="noopener noreferrer"
                                            target="_blank">LA CARTERA – Carlos Vives</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=LTsxQnFt4dM" rel="noopener noreferrer"
                                            target="_blank"> MI AHIJADO – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=gm-0bOtYJCI" rel="noopener noreferrer"
                                            target="_blank">ME GUSTA – Silvestre Dangong </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=fBdnGvwIc-g&ab_channel=BryamReyes" rel="noopener noreferrer"
                                            target="_blank">MATILDE LINA – Alejandro Duran </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=TwKOfVpSeCE" rel="noopener noreferrer"
                                            target="_blank">TU CUMPLEAÑOS – Diomedes Díaz  </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=kbjSigyrdQA" rel="noopener noreferrer"
                                            target="_blank"> NUNCA COMPRENDÍ TU AMOR – Jorge Oñate</a>
                                    </li>
                                </ul>
                            </p>
                        </div>
                        <div className="list">
                            <p>
                                <ul>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=xvVUPGI8Kgo" rel="noopener noreferrer"
                                            target="_blank">QUE DIOS TE BENDIGA – Peter Manjarrés</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=5-wZErH-gj4" rel="noopener noreferrer"
                                            target="_blank">LA PLATA – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=PiqemrJeAPw" rel="noopener noreferrer"
                                            target="_blank">LOS NOVIOS – Alfredo Gutierrez</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=w2nZPLsTs6s" rel="noopener noreferrer"
                                            target="_blank">MAÑANITAS DE INVIERNO – Los Zuleta</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=-9RJkzL7Dtw" rel="noopener noreferrer"
                                            target="_blank">MI PRIMERA CANA – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=SKRSQq8x_eY" rel="noopener noreferrer"
                                            target="_blank">EL SANTO CACHÓN – Los Embajadores</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=BCdy6BIaJaE" rel="noopener noreferrer"
                                            target="_blank">HIJA – Diomedes Díaz </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=bqF8-zp7_sQ" rel="noopener noreferrer"
                                            target="_blank"> AL FIN LLEGASTE TU – Martin Elias</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=E282kuM_YXI" rel="noopener noreferrer"
                                            target="_blank">MI MUCHACHO – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=qn6csJZ6zC4" rel="noopener noreferrer"
                                            target="_blank">OBSESIÓN – Las Estrellas Vallenatas</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=17z3Y1YnRTs" rel="noopener noreferrer"
                                            target="_blank">CÁSATE CONMIGO – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=71-CfcpdbzY" rel="noopener noreferrer"
                                            target="_blank">LOS CAMINOS DE LA VIDA – Los Diablitos</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=XAkN3YHCbhw" rel="noopener noreferrer"
                                            target="_blank">GRACIAS – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=X9H4ul5cstg" rel="noopener noreferrer"
                                            target="_blank">MI HERMANO Y YO – Hermanos Zuleta</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=2NkAofFnCM0" rel="noopener noreferrer"
                                            target="_blank">TODO DE CABEZA – Kaleth Morales</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=Rwqm4nq-hEA" rel="noopener noreferrer"
                                            target="_blank">LA CRECIENTE – Binomio de Oro </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=E3uKBQNbC8s" rel="noopener noreferrer"
                                            target="_blank">DILE – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=Ymbvyfqs_po" rel="noopener noreferrer"
                                            target="_blank">MOMENTOS DE AMOR – Binomio de Oro</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=okiDW3hCbgo" rel="noopener noreferrer"
                                            target="_blank">LLUVIA DE MUJERES – Los Betos</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=JYpXBfiRL4U" rel="noopener noreferrer"
                                            target="_blank">MOSAICO BAILABLE – Alejandro duran.</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=DeT9uEj-yDM&ab_channel=SaborVallenato" rel="noopener noreferrer"
                                            target="_blank">EL MEJORAL – Alejandro Duran</a>
                                    </li>
                                    
                                </ul>
                            </p>
                        </div>
                        <div className="list">
                            <p>
                                <ul>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=WlvEvUiFJyE" rel="noopener noreferrer"
                                            target="_blank"> LA FALLA FUE TUYA – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=tHxtboUhdy4" rel="noopener noreferrer"
                                            target="_blank">AMARTE MAS NO PUDE – Diomedes Díaz</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=-zQ7bEKCmBY" rel="noopener noreferrer"
                                            target="_blank">EL LÁTIGO- Martin elias</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=q9jI8bqJ5L4" rel="noopener noreferrer"
                                            target="_blank">A TU VENTANA – Los chiches </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=1e0vzYczmF0" rel="noopener noreferrer"
                                            target="_blank">DESTROZASTE MI ALMA – Kaleth Morales</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=ELzAEMiOrek" rel="noopener noreferrer"
                                            target="_blank">TU SERENATA – Diomedes Díaz </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=nbp-Uhw6kas" rel="noopener noreferrer"
                                            target="_blank">CÓMO LO HIZO – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=4UtuRcx8T94" rel="noopener noreferrer"
                                            target="_blank">VIVO EN EL LIMBO – Kaleth morales</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=FdqJw0rnGn0" rel="noopener noreferrer"
                                            target="_blank">REGALAME UNA NOCHE – Silvestre Dangond  </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=_3PImSCZF6w" rel="noopener noreferrer"
                                            target="_blank">LA GRINGA- Silvestre  Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=qw4Iy0SrY58" rel="noopener noreferrer"
                                            target="_blank">10 RAZONES PARA AMARTE – Martin elias</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=KP-nL0uEjyQ" rel="noopener noreferrer"
                                            target="_blank">LA LLAMADITA – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=xlfRRxYFeW0" rel="noopener noreferrer"
                                            target="_blank">SEÑORA – Otto Serge</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=ciTU6D_ZnLo" rel="noopener noreferrer"
                                            target="_blank">A BLANCO Y NEGRO – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=v6h6VyrO4bU" rel="noopener noreferrer"
                                            target="_blank">LA COLEGIALA – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=0zR-mZzLw5Y" rel="noopener noreferrer"
                                            target="_blank" >LA CONSENTIDA – Fabian Corrales</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=E3uKBQNbC8s" rel="noopener noreferrer"
                                            target="_blank">DILE – Silvestre Dangond</a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=J49A10Ga0Ew" rel="noopener noreferrer"
                                            target="_blank">CALIDAD DE VIDA – Silvestre Dangond </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/watch?v=zkXuB8xkXmA" rel="noopener noreferrer"
                                            target="_blank">QUE NO SE ENTEREN  – Silvestre Dangond </a>
                                    </li>
                                    
                                      <p style={{color:"white"}}>Y MUCHO MAS...</p>  
                                </ul>
                            </p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    )
}

export default LayoutMusic
