import React from 'react'
import './saludo.css'
import imageprueba from '../../../../images/imageprueba.jpg'
import compartir from '../../../../images/compartir.png'

export default function Saludo() {


    return (
        <div className='body'>
            <div>
                <div className='box-saludo'>
                    <div className='box-img'>
                        <img src={imageprueba} alt='gemelos' width='280px' height="350px" />
                    </div>
                    <div className='box-text'>
                        <div className='title'><p><strong>Grupo vallenato para sus fiestas y eventos</strong></p></div>
                        <div className='text'>
                            <p>
                                Somos un <strong>Grupo vallenato santa marta</strong> o <strong>conjunto vallenato santa marta </strong>
                                llamados <strong>"Los Gemelos del Vallenato"</strong> y estamos dispuestos a ofrecer lo mejor de nuestro
                                folclor vallenato para amenizar con <strong>parranda vallenata</strong> sus cumpleaños, matrimonios, grados,
                                eventos empresariales etc.
                            </p>
                        </div>
                    </div>

                </div>
                <div className="embed">
                    <div className="heading-3"><p><strong>Compartir con:</strong></p></div>
                    <div className='box-embed'>
                        <div className="boton face">
                            <a href="https://www.facebook.com/sharer.php?u=mi-parranda.vercel.app"
                                target="_blank" rel="noopener noreferrer" >
                                Facebook<img src={compartir} width="20px" height="20px" alt="compartir" />
                            </a>
                        </div>
                        <div className="boton wap">
                            <a href="https://api.whatsapp.com/send?text=mi-parranda.vercel.app" target="_blank" rel="noopener noreferrer">
                                Whatsapp<img src={compartir} width="20px" height="20px" alt="compartir" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}