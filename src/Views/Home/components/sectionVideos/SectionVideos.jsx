import React from 'react'
import './sectionVideos.css'
function SectionVideos() {
    return (
        <div className="body">
            <div className='box-videos'>
                <div className="title1">
                    <p>Algunas de nuestras Presentaciones</p>
                </div><br /><br />
                <div className="section">
                        <div className=" ">
                            <iframe width="350px" height="200px" src="https://www.youtube.com/embed/nOFlk4nZSCE"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                        <div className=" ">
                            <iframe width="350px" height="200px" src="https://www.youtube.com/embed/gtWVlXxKczA"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                        <div className=" ">
                            <iframe width="350px" height="200px" src="https://www.youtube.com/embed/6JITe_uwx2k"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                </div>
            </div>
        </div >
    )
}

export default SectionVideos
