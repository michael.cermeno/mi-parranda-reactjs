import React from 'react'
import { Link } from 'react-router-dom'
import Ico from '../../../../Components/ico/Ico.jsx'
import './navbar.css'

export default function Navbar() {

  return (
    
      <nav className="nav ">
          <div className="nav-items">
            <div className="item"><Link to="/"><Ico/></Link></div>
            <div className="item"><Link to="/">Inicio</Link></div>
            <div className="item"><Link to="/service">Nuestro Servicio</Link></div>
            <div className="item"><Link to="/repertoire">Repertorio</Link></div>
            <div className="item"><Link to="/contact">Contactanos</Link></div>
          </div>
          <div className="social">
            <div className="ico"><Link to="">ico</Link></div>
            <div className="ico"><Link to="#">ico</Link></div>
            <div className="ico"><Link to="#">ico</Link></div>
          </div>
      </nav>
    

  )
}