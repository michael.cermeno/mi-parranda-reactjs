import React from 'react'
import Carrusel from '../../Components/Carrusel/Carrusel'
import EnlaceMenssenger from '../../Components/enlace messenger/EnlaceMenssenger'
import Footer from '../../Components/footer/Footer'
import Navbar from '../../Components/Navbar/Navbar'
import TargetHome from '../../Components/targetHome/TargetHome'
import Saludo from './components/sectionSaludo/Saludo'
import SectionVideos from './components/sectionVideos/SectionVideos'




export default function Home() {


    return (
        <div>
            
            <Navbar />
            <Carrusel />
            <Saludo />
            <SectionVideos />
            <TargetHome />
            <Footer />
            <EnlaceMenssenger />
        </div>

    )


}