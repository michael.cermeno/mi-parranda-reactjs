import React from 'react'
import './targetContacts.css'
import Form from '../../../Components/form/Form'
import facebook from '../../../images/facebook.png'
import instagram from '../../../images/instagram.png'
import youtube from '../../../images/youtube.png'
import nequidaviplata from '../../../images/nequidaviplata.png'
function TargetContacts() {
    return (
        <div className='target-layout'>
            <div className='box-target'>
                <div className='photo-accor'>
                    <h1>Contactanos</h1>
                </div>
                <div className="targetyform">
                    <div className="box-contact">
                        <div className="box-social">
                            <h3><strong>Sociales</strong> </h3>
                            <div className="linea"></div>
                            <ul className="ul-list" >
                                <li><a className="icon icon-md icon-gray-7 fa api3 " target="_blank" rel="noopener noreferrer"
                                    href="https://www.facebook.com/Grupo-Musical-Vallenato-En-Santa-Marta-108666374890077/" >
                                    <img src={facebook} height="30px" width="30px" alt="..." />
                                </a>
                                </li>
                                <li>
                                    <a className="icon icon-md icon-gray-7 fa api3" href="https://www.instagram.com/losgemelosdelvallenato/" target="_blank" rel="noopener noreferrer">
                                        <img src={instagram} alt="..." height="30px" width="30px" />
                                    </a>
                                </li>
                                <li>
                                    <a className="icon icon-md icon-gray-7 fa api4" target="_blank" href="https://www.youtube.com/channel/UCCAaNBnZHmpw6_JHuSHQ6WA" rel="noopener noreferrer">
                                        <img src={youtube} alt="..." height="31px" width="30px" />
                                    </a>
                                </li>
                            </ul>
                            <h3><strong>Telefono</strong> </h3>
                            <div className="linea"></div>
                            <h4>📞 300 357 4201</h4>
                            <h3><strong>Abierto</strong> </h3>
                            <div className="linea"></div>
                            <h4>⏱ Las 24 horas</h4>
                            <div class="">
                                <div className="">
                                    <p className=""><strong>¡Realiza tu reserva!</strong></p>
                                    <p> Cotiza tu evento, <strong>Asesoria totalmente ¡gratis!</strong> coloca tus datos y nos
                                        comunicaremos contigo lo mas pronto posible.</p>
                                </div>
                                <div className="">
                                    <p><strong>Formas de pago</strong></p>
                                    <img className="nequi" src={nequidaviplata} alt=".." />

                                </div>
                            </div>
                        </div>
                    </div>
                    <Form />
                </div>
            </div>


        </div>
    )
}

export default TargetContacts
