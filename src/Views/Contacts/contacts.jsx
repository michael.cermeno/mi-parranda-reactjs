import React from 'react'
import EnlaceMenssenger from '../../Components/enlace messenger/EnlaceMenssenger'
import Navbar from '../../Components/Navbar/Navbar'
import TargetContacts from './targetContacts/TargetContacts'
import Footer from '../../Components/footer/Footer'


export default function Contact() {


    return (
        <>
         <Navbar/>
         <TargetContacts/>
         <Footer/>
         <EnlaceMenssenger/>
        </>
       
    )


}