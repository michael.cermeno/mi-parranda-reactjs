import React from 'react'
import './layoutService.css'
function LayoutService() {
    return (
        <div className='box-layout'>
            <div className='layout'>
                <div className='service'>
                    <p>Nuestro Servicio</p>
                </div>
                <div className='box-types'>
                    <div className="types1">
                        <strong> <h1>Serenata Vallenata</h1>
                            <h4>Sorprende con amor</h4><br />
                            <p>
                                1 Hora <br /><br />
                                4 Musicos:<br /><br />
                                Cantante, Acordeon, Caja Vallenata, Guacharaca<br /><br /><br/>
                                Canciones Dedicatorias
                            </p></strong>
                    </div>
                    <div className="types2">
                    <strong> <h1>Parranda Vallenata</h1>
                        <h4>Tipi-Bajo</h4><br />
                        <p>
                            1 Hora <br /><br />
                            5 Musicos:<br /><br />
                            Cantante, Acordeon, Caja Vallenata, Guacharaca y Bajo<br /><br/><br/>
                            Repertorio Nueva Ola
                        </p></strong>
                    </div>
                    <div className="types3">
                    <strong> <h1>Concierto</h1><br/>
                        <h4>Grupo Vallenato Completo</h4><br />
                        <p>
                            1 Hora <br /><br />
                            8 Musicos:<br /><br />
                            Cantante, Acordeon, Caja Vallenata, Guacharaca, Congas, Guitarra, Bajo y Timbales<br /><br />
                            Repertorio Nueva Ola
                        </p></strong>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LayoutService