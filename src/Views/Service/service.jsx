import React from 'react'
import Navbar from '../../Components/Navbar/Navbar'
import LayoutService from './components/LayoutService'
import Footer from '../../Components/footer/Footer'
import EnlaceMessenger from '../../Components/enlace messenger/EnlaceMenssenger'
export default function Service() {

    return (
        <>
            <Navbar />
            <LayoutService />
            <Footer/>
            <EnlaceMessenger/>
        </>
    )

}