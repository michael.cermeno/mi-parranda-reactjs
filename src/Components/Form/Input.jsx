import React from 'react'


const Input = ({ label, id, type = 'text', error, register, required = false }) => {
    
    return (
        <>
            <div className={`form-part ${error?.type ? 'has-error' : ''}`} >
                <label for={id}><strong>{label}</strong></label>
                <input
                    id={id}
                    type={type}
                    {...register(id, { required })}
                    
                />
            </div>
            {error?.type === 'required' && <span>Este campo es requerido</span>}
        </>
    )
}

export default Input