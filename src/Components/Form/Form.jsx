import React, {   useContext, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import './form.css'
import loading from "../../images/loading.gif"
import Input from './Input'
import ContextStore from '../../Store/ContextStore'


function Form() {
    const { setForm, sendData } = useContext(ContextStore)
    const [isSend, setIsSend] = useState(false)
    const [isOk, setIsOk] = useState(false)
    const { register, handleSubmit, formState: { errors } } = useForm()
    //  const regexNumber = /^[0-9]+$/
    const handleReset = useRef("")
    const buttonEl  = useRef("")
    
    function sendSubmit(data) {
        setForm(JSON.stringify(data))
        setIsSend(true)
      //  setTimeout(() => {
        //    setIsSend(false)
        //}, 3000);
        sendData()
        setTimeout(() => {
            setIsOk(true)
        }, 3001);
        handleReset.current.click()
        buttonEl.current.disabled = true
    }

    return (
        <div className="formulario">
            <form className="box-form" onSubmit={handleSubmit(sendSubmit)} >
                <Input
                    id="date"
                    label="Fecha del evento"
                    type="datetime-local"
                    error={errors.date}
                    register={register}
                    required={true}
                />
                <Input
                    id="name"
                    label="Tu Nombre"
                    error={errors.name}
                    register={register}
                    required={true}
                />
                <Input
                    id="cellPhone"
                    label="Numero Celular"
                    error={errors.cellPhone}
                    register={register}
                    required={true}
                    type="number"
                />
                <Input
                    id="address"
                    label="Barrio del Evento"
                    error={errors.address}
                    register={register}
                    required={true}
                />               
                <div className="form-part" >
                    <label className="" for='description'><strong>Tu Mensaje</strong></label>
                    <textarea className="" {...register("description")} id="description">Estoy interesado en su servicio ...</textarea>
                </div>
                <div className="form-part">
                    <button className=""  ref={buttonEl} type="submit" ><strong>Reserva tu evento</strong></button>
                </div>
                <button type='reset' ref={handleReset} style={{display:"none"}} ></button>
            </form>
            {isSend && <div className="sending">Enviando <img src={loading} width="40px" height='40px' alt='..' ></img></div>}
            {isOk && <div className="send-ok" onClick={()=>{setIsOk(false);setIsSend(false)}}>Gracias por Escribirnos, Pronto nos comunicaremos contigo 😊</div>}
            
        </div>
    )
}

export default Form
