import React from 'react'
import Form from '../form/Form'

import './targetHome.css'


export default function TargetHome() {
  

    return (
        <section className="form">
            <div className="space">
                <div className="form-box">
                    <div className="target">
                        <div className="texto">
                            <p className="head1">Realiza tu reserva!</p>
                            <p className='parrafo'> Cotiza tu evento, <strong>Asesoria totalmente ¡gratis!</strong> coloca tus datos y nos
                                comunicaremos
                                contigo lo mas pronto posible.</p>
                        </div>
                    </div>
                   <Form/>
                </div>
            </div>
        </section >
    )
}