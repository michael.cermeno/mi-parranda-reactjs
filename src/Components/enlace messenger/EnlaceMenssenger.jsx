import React, { useState, useEffect } from 'react'
import './enlaceMessenger.css'
import whatsapp from '../../images/whatsapp.png'
import messenger from '../../images/messenger.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowAltCircleUp } from '@fortawesome/free-regular-svg-icons'
import { useWindowScroll } from 'react-use'

function EnlaceMenssenger() {
    const [visible, setVisible] = useState(false)


    const { y } = useWindowScroll()
    useEffect(() => {
        if (y > 249) {
            setVisible(true)
        } else {
            setVisible(false)
        }
    }, [y])
    function handleClick() {
        window.scrollTo({ top: 0, behavior: "smooth" })
    }


    return (
        <>
            <div className='icon-face'>
                <a href="https://m.me/108666374890077" target="blank" rel="noopener noreferrer">
                    <img src={messenger} height="45px" width="45px" alt="" />
                </a>
            </div>
            <div className='icon-whap'>
                <a href="https://wa.me/573003574201?text=Me%20interesa" target="_BLANK" rel="noopener noreferrer">
                    <img alt=".." src={whatsapp} height="45px" width="45px" />
                </a>
            </div>

            {visible && <div className='up'>
                <FontAwesomeIcon icon={faArrowAltCircleUp} className='icon-up' onClick={handleClick}></FontAwesomeIcon>
            </div>}
            
        </>
    )
}

export default EnlaceMenssenger
