import React from 'react'
import Notfound from '../../images/Notfound.jpg'
function NotFound() {
    return <img src={Notfound} alt="Notfound" width="100%"/>
    
}

export default NotFound
