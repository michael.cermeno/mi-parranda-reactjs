import React, { useState, useEffect, useRef } from 'react'
import { useLocation } from 'react-router-dom'
import { Link } from 'react-router-dom'
import logo from '../../images/logo.png'
import './navbar.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faInstagram, faYoutube, } from '@fortawesome/free-brands-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { useWindowScroll } from 'react-use'

export default function Navbar() {

  const location = useLocation()
  const page = location.pathname

  ///////navbar
  const [navOff, setNavOff] = useState(false)
  const { y } = useWindowScroll()

  const [toggle, setToggle] = useState(false)
  useEffect(() => {
    if (y > 0) {
      setNavOff(true)
    } else {
      setNavOff(false)
    }
  }, [y])
  //////scroll to up

  const span = useRef(null)
  const social = useRef(null)
  useEffect(() => {
    
    if (toggle) {
      span.current.style.top = "100px"
      social.current.style.top = "100px"
    } else {
      span.current.style.top = "-300vw"
      social.current.style.top = "-300vw"
    }
  }, [toggle])
  function handleClick() {
    window.scroll({ top: 0, behavior: "instant" })
  }
  function handleClickSpan(e) {

    setToggle(!toggle)

  }
  return (
    <header className={y > 1 && 'top-space'}>
      <nav className={!navOff ? "nav" : "nav nav-fixed"}>
        {!toggle && <div className="span" onClick={handleClickSpan}><FontAwesomeIcon icon={faBars} className='bars' /></div>}
        {toggle && <div className="span" onClick={handleClickSpan}><FontAwesomeIcon icon={faTimes} className='bars' /></div>}
        <div className="logo-box"><Link to="/" onClick={handleClick}><img src={logo} alt=".."></img></Link></div>
        <div className="nav-items  " ref={span}>
          <div className={page === "/" ? "itemActived" : "item"}><Link to="/" onClick={handleClick} >Inicio</Link></div>
          <div className={page === "/service" ? "itemActived" : "item"}><Link to="/service" onClick={handleClick}>Nuestro Servicio</Link></div>
          <div className={page === "/repertoire" ? "itemActived" : "item"}><Link to="/repertoire" onClick={handleClick}>Repertorio</Link></div>
          <div className={page === "/contact" ? "itemActived" : "item"}><Link to="/contact" onClick={handleClick} >Contactanos</Link></div>
        </div>
        <ul className="social" ref={social}>
          <li className='Pa'>
            <a href="https://www.instagram.com/losgemelosdelvallenato/" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faInstagram} className='icons' />
              <span></span><span></span><span></span><span></span></a>
          </li>
          <li className='Pa'>
            <a href="https://www.facebook.com/Grupo-Musical-Vallenato-En-Santa-Marta-108666374890077/" target="_blank"
              rel="noopener noreferrer"><FontAwesomeIcon icon={faFacebookF} className='icons' />
              <span></span><span></span><span></span><span></span></a></li>
          <li className='Pa' >
            <a href="https://www.youtube.com/channel/UCCAaNBnZHmpw6_JHuSHQ6WA" target="_blank"
              rel="noopener noreferrer"><FontAwesomeIcon icon={faYoutube} className='icons' />
              <span></span><span></span><span></span><span></span></a></li>
        </ul>

      </nav>

    </header>
  )
}