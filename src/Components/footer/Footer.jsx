import React from 'react'
import { Link } from 'react-router-dom'
import Ico from '../ico/Ico'
import './footer.css'
import compartir from '../../images/compartir.png'
function Footer() {
    const year = new Date().getFullYear()

    return (
        <section className='box-footer'>
            <footer className="footer">
                <div className="box-ico">
                    <Link to="/">
                        <Ico />
                    </Link>
                </div>
                <div className="embed2">
                    <div className="boton face">
                        <a href="https://www.facebook.com/sharer.php?u=mi-parranda.vercel.app" rel="noopener noreferrer"
                            target="_blank" >
                            Facebook<img alt=".. " src={compartir} width="20px" height="20px" />
                        </a>
                    </div>
                    <div className="boton wap">
                        <a rel="noopener noreferrer" href="https://api.whatsapp.com/send?text=mi-parranda.vercel.app"
                            target="_ blank">
                            Whatsapp<img alt=".. " src={compartir} width="20px" height="20px" />
                        </a>
                    </div>
                </div>
                <p>Derechos Reservados &copy; {year}</p>
            </footer>
        </section>
    )
}

export default Footer