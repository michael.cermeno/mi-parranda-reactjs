
import React from 'react'


export default function Input({atribute , onChange}){
    
    
    return(
        <input
        className={atribute.className}
        type={atribute.type}
        placeholder={atribute.placeholder}
        name={atribute.name}
        onChange={(e) => onChange(e.target.name, e.target.value)}
        />
    )
}