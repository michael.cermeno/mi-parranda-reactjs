import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import image1 from '../../images/image1.jpg'
import image2 from '../../images/image2.jpg'
import image3 from '../../images/image3.jpg'
import image4 from '../../images/image4.jpg'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import './carrusel.css'

export default function Carrusel() {
   
 

  return (
    <section className='carucel'>
      <div id="carouselExampleIndicators" class="carousel slide carucel" data-bs-ride="carousel">
        <div className="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={image1} height="540px" width="900px" className="d-block carucel-img " alt="..." />
          </div>
          <div className="carousel-item">
            <img src={image2} height="540px" width="900px" className="d-block carucel-img" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={image3} height="540px" width="900px" className="d-block carucel-img" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={image4} height="540px" width="900px" className="d-block carucel-img" alt="..." />
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span className="" aria-hidden="true"><FontAwesomeIcon className='left' icon={faChevronLeft}/></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span className="" aria-hidden="true"><FontAwesomeIcon  className='right'icon={faChevronRight}/></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </section>

  )

}