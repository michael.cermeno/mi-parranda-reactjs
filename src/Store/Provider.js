import React, { useState } from 'react'
import ContextStore from './ContextStore'
import Apicall from '../Components/api/Apicall'

function Provider({ children }) {
    const FORM_ENPOINDT = 'https://1r1djs4u0c.execute-api.us-east-1.amazonaws.com/Prod/eventos'
    const [form, setForm] = useState({})


    async function sendData() {
        try {
            const Data = await Apicall({
                url: FORM_ENPOINDT,
                body: new FormData(form)
            })
            if (Data.status === 200) {
                console.log("formulario exitoso")
            } else if(Data.status>399){
                console.log("formulario no exitoso")
            }
        } catch (err) {
            console.log(err)
        }
    }





    return (
        <ContextStore.Provider value={{ setForm, sendData }}>
            {children}
        </ContextStore.Provider>
    )
}

export default Provider
