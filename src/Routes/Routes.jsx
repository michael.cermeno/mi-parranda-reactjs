import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "../Views/Home/Home"
import Service from "../Views/Service/Service"
import ListMusic from "../Views/ListMusic/ListMusic"
import Contact from "../Views/Contacts/Contacts"
import For0for from "../Views/For0for/For0for"

export default function Routes() {


    return (
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Home />
                </Route>
                <Route path="/service" exact>
                    <Service />
                </Route>
                <Route path="/repertoire" exact>
                    <ListMusic />
                </Route>
                <Route path="/contact" exact>
                    <Contact />
                </Route>
                <Route >
                    <For0for />
                </Route>
            </Switch>
        </Router>
    )
}