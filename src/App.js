
import './App.css';
import Routes from './Routes/Routes';
import Provider from './Store/Provider';

function App() {
  return (
    <div className='todo'>
      <Provider>
        <Routes />
      </Provider>
    </div>
  );
}

export default App;
